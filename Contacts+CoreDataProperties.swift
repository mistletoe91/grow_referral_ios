//
//  Contacts+CoreDataProperties.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-04.
//  Copyright © 2018 Grow Referral. All rights reserved.
//
//

import Foundation
import CoreData


extension Contacts {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Contacts> {
        return NSFetchRequest<Contacts>(entityName: "Contacts")
    }

    @NSManaged public var name: String?
    @NSManaged public var phone: String?

}
