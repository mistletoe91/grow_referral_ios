//
//  ContactsTableViewCell.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-06.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {
    @IBOutlet weak var ctlButtonInvite: UIButton! 
    @IBOutlet weak var ctlContactName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib() 
        // Initialization code
    }
    
    @IBAction func onButtonClick(_ sender: UIButton) {
        print("button Clicked " );
        print(sender.tag) 
    } 
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
