//
//  gr.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-04.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit

class gr: NSObject {
    func gr_authenticate (email: String, password: String) -> Bool{
        return (isValidEmail( str : email)) ;
    }//end function
    func isValidEmail(str:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: str)
    } 
}
