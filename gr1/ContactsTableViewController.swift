//
//  ContactsTableViewController.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-06.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController: UITableViewController,UISearchBarDelegate,UISearchDisplayDelegate  {
    
    @IBOutlet weak var uiSearchBar: UISearchBar!
    @IBOutlet var ctlTableView: UITableView!
    
    var myContacts:[[String]] = []
    var searchController: UISearchController!
    var filterdContacts:[[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
        /* START core data */
        //Get the NSManagedObjectContext : The hole in container
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Contacts")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let d_name = (data.value(forKey: "name") as! String)
                //let d_phoneNumbers = (data.value(forKey: "name") as! String)
                //myContacts
                myContacts.append([d_name, "1" ,"2"])
            }
            filterdContacts = myContacts
        } catch {
            print("Failed")
        }
        /* End Core Data */
    }
    
    // Search Bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // If we haven't typed anything into the search bar then do not filter the results
        if searchText == "" {
            filterdContacts = myContacts
        } else {
            // Filter the results
            filterdContacts = myContacts.filter { $0[0].lowercased().contains(searchText.lowercased()) }
        }
        self.ctlTableView.reloadData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsTableViewCell", for: indexPath)
            as! ContactsTableViewCell
        cell.ctlContactName?.text = filterdContacts[indexPath.row][0] as? String
        cell.ctlButtonInvite.setTitle("Invite", for:UIControl.State.normal )
        cell.ctlButtonInvite.tag = indexPath.row;
        
        //cell.ctlButtonInvite.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)

        /*
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "contactsTableViewCell")
        cell.textLabel?.text =  filterdContacts[indexPath.row][0] as? String
         */
        return(cell)
    }
    /*
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        print(sender.tag) 
    }
 */
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return filterdContacts.count
    }

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
