//
//  ImportContactsViewController.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-04.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit
import Contacts

class ImportContactsViewController: UIViewController {

    
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var ctlTitle: UILabel!
    private var totalContacts = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        

    }
    
    @IBAction func fnImportContactsAndSaveInCoreData(_ sender: Any) {
        
        //Get the NSManagedObjectContext : The hole in container
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = CNContactFetchRequest(keysToFetch: [
            CNContactFamilyNameKey as CNKeyDescriptor,
            CNContactGivenNameKey as CNKeyDescriptor,
            CNContactPhoneNumbersKey as CNKeyDescriptor
            ])
        
        //var countImported = 0;
        try! CNContactStore().enumerateContacts(with: req) {
            contact, stop in
            
            self.totalContacts += 1
            self.counterLabel.text = "Importing \(self.totalContacts) Contacts";
            
            print(contact.givenName);
            //print(contact.phoneNumbers)
            let context_Contact = Contacts(context: context)
            context_Contact.name = contact.givenName
            
            for phone in contact.phoneNumbers {
                // phone.value.initialCountryCode
                print( phone.value.stringValue );
                context_Contact.phone = phone.value.stringValue
            }
            //countImported += 1;
            
            //Save The Data
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            do {
                try context.save()
                
                //Everything is good - Goto next page
                let next = self.storyboard?.instantiateViewController(withIdentifier:   "AfterContactsImporterdViewController") as! AfterContactsImporterdViewController
                self.present(next, animated: true, completion: nil)
                
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        
        }//end try
    }//end function fnImportContactsAndSaveInCoreData
    
    
    
}
