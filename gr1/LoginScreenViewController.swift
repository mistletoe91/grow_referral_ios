//
//  LoginScreenViewController.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-04.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController {

    //let username:string;
    //let password:string;
    @IBOutlet weak var txtusername: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
    //var grObject:NSObject;
    let grObject =  gr ();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginSubmit(_ sender: Any) {
        
        //For testing
        txtusername.text = "abc@dasd.com";
        txtpassword.text = "a123123";
        
        if grObject.gr_authenticate  (email : txtusername.text!, password:txtpassword.text!) {
            
            //Redirect user to Next Page
            var next = self.storyboard?.instantiateViewController(withIdentifier:   "ImportContactsViewController") as! ImportContactsViewController
            self.present(next, animated: true, completion: nil)
            
        } else {
            
            //Show Alert
            var alert = UIAlertController(title: "Wrong Email", message: "Please enter valid Email", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try Again!", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }//endif
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
