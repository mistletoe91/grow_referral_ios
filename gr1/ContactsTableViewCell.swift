//
//  ContactsTableViewCell.swift
//  gr1
//
//  Created by MACBOOK PRO on 2018-10-06.
//  Copyright © 2018 Grow Referral. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {
 
    
    @IBOutlet weak var ctlContactName: UILabel!
    @IBOutlet weak var ctlContactPhone: UILabel!
    var quotes = [ContactCell]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
